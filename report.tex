\documentclass[11pt]{article}

\usepackage{
  amsmath,
  amssymb,
  booktabs,
  graphicx,
  siunitx,
  subfigure,
  textcomp,
  todonotes,
  url,
  xspace
}
\usepackage[utf8]{inputenc}
\usepackage[bf]{caption}
\usepackage[
backend=bibtex,
style=numeric,
natbib=true,
url=false, 
doi=true,
eprint=false
]{biblatex}

\addbibresource{sources.bib}

\newcommand{\eg}{e.g.,\xspace}
\newcommand{\bigeg}{E.g.,\xspace}
\newcommand{\etal}{\textit{et~al.\xspace}}
\newcommand{\etc}{etc.\@\xspace}
\newcommand{\ie}{i.e.,\xspace}
\newcommand{\bigie}{I.e.,\xspace}


\title{Dead time Calculation}
\author{Christian Bourjau}

\begin{document}
\maketitle

Modern collider based experiments are capable of producing many orders of magnitude more data than is possible or feasible to process further.
Hence, it is crucial to decide rapidly during the data acquisition (DAQ) if an event qualifies for such further processing or if it should essentially been discarded. 

Another common problem is that faster read out electronics occupy more space in the detector and dissipate more heat than slower ones.
It is therefore desirable to use the available bandwidth of the readout electronics as efficiently as possible.
Since triggered events are by nature randomly distributed, one can maximize the efficiency of the readout by means of a \emph{first in first out} (FIFO) buffer.
A FIFO buffer can temporarily hold a small number of events before writing them to disk.
It is sometimes also regarded as a \emph{leaky bucket} which empties (\ie{} reads out events) at a constant time $\tau_{\text{readout}}$ while being randomly filled with a mean rate of $R_{\text{trigger}}$. 

The interplay between the trigger and the FIFO is depicted in figure~\ref{fig:schema}.
From the sensor, the analog signal is fast tracked to the trigger while being delayed on its way to the the Analog-Digital converter (ADC).
At every bunch crossing (BX), the trigger is checked.
If the trigger fires, it passes a signal to the ADC that the event should be read out.
This readout requires the time $\tau_{\text{ADC}}$ during which the ADC cannot process any new triggers. 
Once read out, the signal is passed on to the FIFO buffer.
If the buffer is completely filled, a busy signal blocks the ADC from reading out any more events, until the buffer has vacancies again.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{./figures/trigger_setup.png}
  \caption{Logical schematics of the DAQ \cite{silverstein15:_intro}. At every bunch crossing, the detector sensors are read out to the trigger. If the trigger fires and if the FIFO buffer is not full, the event is read out by the ADC and forwarded to the FIFO from where it is written to storage.}\label{fig:schema}
\end{figure}

The overall efficiency of a trigger system is gauged by the \emph{dead time} $D$ defined as
\begin{equation}
  \label{eq:1}
  D = 1 - \frac{N_{\text{total}}}{N_{\text{ADC}}+nN_{\text{FIFO}}}
\end{equation}

where, in a given event sample, $N_{\text{total}}$ is the total number of triggers fired, $N_{\text{ADC}}$ are the triggers lost due to the readout time of the ADC and $N_{\text{FIFO}}$ is the number triggers lost due to the FIFO being full.
The purpose of this exercise is to simulate the dead time of a simplified version of the LAr calorimeter trigger used in ATLAS.
The assumptions for the simulated system are
\begin{itemize}
\item Constant bunch crossing rate of $R_{\text{BX}} = \SI{40}{MHz}$ (\SI{25}{ns} intervals)
\item Trigger acceptance rate of $R_{\text{trigger}} = \SI{75}{kHz}$
\item ADC read out time $\tau_{ADC} = \SI{4}{BX}$
\item FIFO to storage read out time $\tau_{FIFO} = \SI{424}{BX}$
\end{itemize}

Furthermore, the lecture notes pointed out that an analytic solution exists for the FIFO in the case of a constant readout time\cite{heath89:_dead}:
\begin{align}
  \label{eq:2}
  D_N &= 1 - \frac{S_N}{1+\rho} \\
  S_N &= e^{N\rho} \sum_{j=0}^{N-1} \frac{{(N-j)}^j {(-\rho e^{-\rho})}^{j}} {j!}
\end{align}
where $N$ denotes the total buffer size minus 1 and $\rho = R \tau$.
Albeit, this solution does not take into account the ADC readout time, it still provides a good check for the MC simulation for small buffer sizes.

The deadtime of the isolated ADC (disregarding any busy signals from the FIFO), can also be evaluated analytically:
\begin{equation}
  \label{eq:3}
  D_{\text{ADC}} = 1 - (1 - \frac{R_{\text{trigger}}} {R_{\text{BX}}})^{\tau_{BX}\cdot\SI{1}{BX}}
\end{equation}
which evaluates to $D_{\text{ADC}} = 0.00748$ with the above parameters. 

Given the above, the posed exercise problem was to find the minimum FIFO buffer size required to achieve a dead time of less than $1\%$.

Figure~\ref{fig:plot} displays the results of the MC simulations of the system with and without an ADC readout time.
Furthermore, the analytic solution of the FIFO, $D_{ADC}$ and the goal of $1\%$ are depicted.
As can be seen the latter threshold is passed, when including the ADC, at a FIFO buffer size of $N=11$.
Furthermore, the figure shows how the the buffer size is the dominant factor while being small.
For large buffer sizes, the ADC readout time becomes the limiting factor, to which the MC simulation converges for increasing $N$.

Based on these results, the choice of the $1\%$ goal becomes clear: It marks the point where the FIFO buffer size becomes irrelevant and the deadtime is solely dominated by the fixed ADC readout time. Beyond this point, any further increase of the buffer size will not yield a further decrease in deadtime, but possibly deteriorate other performance parameters of the detector.

\begin{figure}
  \centering
  \includegraphics[scale=1]{./figures/simulation.pdf}
  \caption{Results of the MC simulation and analytical solutions for the FIFO buffer problem. The deadtime including the ADC drops below $0.01$ for a FIFO buffer size of $11$.}
  \label{fig:plot}
\end{figure}

\printbibliography

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
