from math import exp, factorial
import random

from ROOT import TF1
from rootpy.plotting import Hist1D

from roofi import figure


def Sn(N, roh):
    sum = 0
    for j in range(N):
        sum += ((N - j) ** j * (-roh * exp(-roh)) ** j) / factorial(j)
    sum *= exp(N * roh)
    return sum


def deadtime(buffers, tau, r):
    # the following -1 is from the lecture note!!!!
    n = buffers - 1
    roh = tau * r  # 75e3 * 10.6e-6
    sn = Sn(n, roh)
    dn = 1 - sn / (1 + roh * sn)
    return dn


class Trigger(object):
    def __init__(self):
        self.previous_trigger = -1000
        self.readout_time = None

    def process_bx(self, bx):
        if self.previous_trigger + self.readout_time < bx:
            self.previous_trigger = bx
            return True
        else:
            return False


class Fifo(object):
    def __init__(self):
        self.event_buffer = []

    def process_bx(self, bx):
        # remove expired events
        while len(self.event_buffer) > 0:
            if self.event_buffer[0] < bx:
                self.event_buffer.pop(0)
            else:
                break

        # add new event; add the bx value when it will be finished
        if len(self.event_buffer) < self.buffer_length:
            # how long do we have to wait for the previous events to finish?
            # either until the previous one finishes + readout_time or, if it the first in the list, until bx + readout_time
            try:
                this_bx_finished = self.event_buffer[-1] + self.readout_time
                if this_bx_finished < bx:
                    print "asdf"
            except IndexError:
                this_bx_finished = bx + self.readout_time
            self.event_buffer.append(this_bx_finished)
            return True
        else:
            return False

#trigger = Trigger()
#trigger.readout_time = 1

prob = 0.001875
num_bx = 10000000
buffer_lengths = range(2, 21)
hist_setup = (19, 1.5, 20.5)
total_triggers_hist = Hist1D(*hist_setup)

missed_triggers_hist = Hist1D(*hist_setup)
analytical_hist = Hist1D(*hist_setup)
total_triggers_hist.Sumw2(True)
missed_triggers_hist.Sumw2(True)

for buffer_length in buffer_lengths:
    fifo = Fifo()
    fifo.buffer_length = buffer_length
    fifo.readout_time = 424
    missed_events = 0
    total_triggers = 0
    for bx in xrange(num_bx):
        if random.random() > prob:
            continue
        total_triggers_hist.fill(buffer_length, 1)
        # if not trigger.process_bx(bx):
        #     missed_events += 1
        #     continue
        if not fifo.process_bx(bx):
            missed_triggers_hist.fill(buffer_length, 1)

    analytical_hist.SetBinContent(
        analytical_hist.FindBin(buffer_length),
        deadtime(fifo.buffer_length, prob, fifo.readout_time))
    analytical_hist.SetBinError(
        analytical_hist.FindBin(buffer_length),
        0)

analytical_hist.drawstyle = "P"
final_hist = missed_triggers_hist / total_triggers_hist

fig = figure.Figure()
fig.style = figure.Styles.Public_full
fig.xtitle = "Buffer size N"
fig.ytitle = "Deadtime D_{N}"
fig.legend.position = 'tr'
fig.plot.logy = True
fig.plot.ymin = 0.0001
fig.plot.palette = 'colorblind'
fig.add_plottable(final_hist, legend_title="MC deadtime; no L1A")
fig.add_plottable(analytical_hist, legend_title="Analytical solution; no L1A")


#########################################
#             With L1A                  #
#########################################
l1a_readout = 4

total_triggers_hist = Hist1D(*hist_setup)
missed_triggers_hist = Hist1D(*hist_setup)

total_triggers_hist.Sumw2(True)
missed_triggers_hist.Sumw2(True)

for buffer_length in buffer_lengths:
    fifo = Fifo()
    fifo.buffer_length = buffer_length
    fifo.readout_time = 424
    missed_events = 0
    total_triggers = 0
    previous_trigger = -100
    for bx in xrange(num_bx):
        if random.random() > prob:
            continue
        total_triggers_hist.fill(buffer_length, 1)

        # L1A
        if not (previous_trigger + l1a_readout < bx):
            # we are currently dead
            missed_triggers_hist.fill(buffer_length, 1)
            continue
        else:
            previous_trigger = bx

        # FIFO
        if not fifo.process_bx(bx):
            missed_triggers_hist.fill(buffer_length, 1)

final_hist = missed_triggers_hist / total_triggers_hist
fig.add_plottable(final_hist, legend_title="MC deadtime; with L1A")

f = TF1("1percLine", "0.01", 0, 20)
fig.add_plottable(f, legend_title="1% deadtime")
f = TF1("L1Ar_limit", "0.00747893", 0, 20)
fig.add_plottable(f, legend_title="ADC Limit")


fig.save_to_file("./", "simulation.pdf")
#c = fig.draw_to_canvas()
#c
